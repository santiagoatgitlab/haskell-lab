import System.Environment
import System.Directory
import Data.List

weeksDir = "/usr/lib/premier/matches/"

data Team = Team { 
    getName :: String,
    getMatches :: Int,
    getWon :: Int,
    getLost :: Int,
    getGoalsScored :: Int,
    getGoalsConceded :: Int
    }
    deriving Eq

data TeamMatch = TeamMatch { 
    getTeamName :: String,
    getMatchPoints :: Int,
    getMatchGoalsScored :: Int,
    getMatchGoalsConceded :: Int
    }
    deriving Eq

instance Show Team where
    show (Team n tm wm lm gs gc) = let 
        pad name    = name ++ (concat $ replicate (18 - length name) " ")
        padNumber n = (concat $ replicate (3 - (length $ show n)) " ") ++ show n 
        points      = (wm*3) + dm
        dm          = tm-wm-lm
        gDiff       = gs-gc
        in "│" ++ pad n ++ "│" ++ padNumber points ++ "│" ++ padNumber tm ++
           "│" ++ padNumber wm ++ "│" ++ padNumber dm ++
           "│" ++ padNumber lm ++ "│" ++ padNumber gs ++
           "│" ++ padNumber gc ++ "│" ++ padNumber gDiff ++ "│"

-- the fourth criteria for comparison (getName) being inverted is on purpose. Think about it
sortByPoints :: Team -> Team -> Ordering
sortByPoints t1 t2 = compare (teamPoints t2, diff t2, getGoalsScored t2, getName t1)
                             (teamPoints t1, diff t1, getGoalsScored t1, getName t2)

sortByWm :: Team -> Team -> Ordering
sortByWm t1 t2 = compare (getWon t2, teamPoints t2, diff t2, getGoalsScored t2, getName t1)
                         (getWon t1, teamPoints t1, diff t1, getGoalsScored t1, getName t2)

sortByLm :: Team -> Team -> Ordering
sortByLm t1 t2 = compare (getLost t1, teamPoints t2, diff t2, getGoalsScored t2, getName t1)
                         (getLost t2, teamPoints t1, diff t1, getGoalsScored t1, getName t2)

sortByGs :: Team -> Team -> Ordering
sortByGs t1 t2 = compare (getGoalsScored t2, teamPoints t2, diff t2, getName t1)
                         (getGoalsScored t1, teamPoints t1, diff t1, getName t2)

sortByGc :: Team -> Team -> Ordering
sortByGc t1 t2 = compare (getGoalsConceded t1, teamPoints t2, diff t2, getGoalsScored t2, getName t1)
                         (getGoalsConceded t2, teamPoints t1, diff t1, getGoalsScored t1, getName t2)

sortByDiff :: Team -> Team -> Ordering
sortByDiff t1 t2 = compare (diff t2, teamPoints t2, getGoalsScored t2, getName t1)
                           (diff t1, teamPoints t1, getGoalsScored t1, getName t2)

argToInt :: [String] -> Int
argToInt []    = 0
argToInt (s:_) = read s :: Int

diff t       = getGoalsScored t - getGoalsConceded t
teamPoints t = ((*3) $ getWon t) + (getMatches t - getWon t - getLost t)

split [] sep = []
split l sep  = foldl splitInt [""] $ reverse l
    where splitInt [] cur = [[cur]]
          splitInt (last:acc) cur
              | cur == sep = "":last:acc
              | otherwise  = (cur:last):acc

toTeam :: TeamMatch -> Team
toTeam (TeamMatch n p gs gc) = let wonMatches  = if p == 3 then 1 else 0
                                   lostMatches = if p == 0 then 1 else 0
                               in Team n 1 wonMatches lostMatches gs gc

addWeek :: [Team] -> [TeamMatch] -> [Team]
addWeek [] week   = map toTeam week
addWeek clas week = map addValues clas
    where addValues (Team n tm wm lm gs gc) = let
              isTeam (TeamMatch matchTeamName _ _ _) = n==matchTeamName
              maybeTM = find isTeam week
              getTM (Just tm) = tm
              newTeam 
                  | maybeTM /= Nothing = let
                      newWm = if (getMatchPoints $ getTM maybeTM) == 3 then wm + 1 else wm
                      newLm = if (getMatchPoints $ getTM maybeTM) == 0 then lm + 1 else lm
                      newGs = gs + (getMatchGoalsScored $ getTM maybeTM)
                      newGc = gc + (getMatchGoalsConceded $ getTM maybeTM)
                      in Team n (tm + 1) newWm newLm newGs newGc
                  | otherwise = Team n tm wm lm gs gc
              in newTeam 

parseLine matchLine  = (teamMatch1,teamMatch2)
    where teamMatch1 = TeamMatch (name1) (getPoints goals1 goals2) goals1 goals2
          teamMatch2 = TeamMatch (name2) (getPoints goals2 goals1) goals2 goals1
          team1      = (split matchLine ',') !! 0
          team2      = (split matchLine ',') !! 1
          name1      = (split team1 ':') !! 0
          name2      = (split team2 ':') !! 0
          goals1     = read ((split team1 ':') !! 1) :: Int
          goals2     = read ((split team2 ':') !! 1) :: Int
          getPoints goalsS goalsC
              | goalsS > goalsC = 3
              | goalsS < goalsC = 0
              | otherwise       = 1

getTeamMatches :: [String] -> [TeamMatch] -> [TeamMatch]
getTeamMatches [] result     = result
getTeamMatches (x:xs) result = getTeamMatches xs (fst(parseLine x):snd(parseLine x):result)

getWeekFiles = do
    args <- getArgs
    dirs <- listDirectory weeksDir
    return $ let weeksNumber = argToInt args
             in if weeksNumber > 0
                then take weeksNumber $ sort dirs
                else sort dirs
getWeek file = do
    contents <- readFile (weeksDir ++ file)
    return $ getTeamMatches (lines contents) []

readAllWeeks :: [String] -> IO [[TeamMatch]]
readAllWeeks []     = do return []
readAllWeeks (x:xs) = do
    week <- (getWeek x)
    next <- (readAllWeeks xs)
    return (week:next)

getClasification :: IO [Team]
getClasification = do
    weekFiles <- getWeekFiles
    weeks     <- readAllWeeks weekFiles
    return $ foldl addWeek [] weeks

sortTeams :: String -> [Team] -> [Team]
sortTeams criteria teams
    | criteria == "wm"   = sortBy sortByWm teams
    | criteria == "lm"   = sortBy sortByLm teams
    | criteria == "gs"   = sortBy sortByGs teams
    | criteria == "gc"   = sortBy sortByGc teams
    | criteria == "diff" = sortBy sortByDiff teams
    | otherwise          = sortBy sortByPoints teams

firstArg :: [String] -> String
firstArg []            = "points"
firstArg (criteria:[]) = if elem criteria ["points","wm","lm","gs","gc","diff"]
                         then criteria
                         else error $ "ordering criteria (" ++ criteria ++ ") not valid"

getOrdered :: String -> [Team] -> [String]
getOrdered criteria teams = let
             ordered = sortTeams criteria teams
             pad n   = (concat $ replicate (3 - (length $ show n)) " ") ++ show n 
              in map (\(i,t) -> "│" ++ pad i ++ show t) $ zip [1..] ordered

main = let 
    top    = "    ┌──────────────────┬───┬───┬───┬───┬───┬───┬───┬───┐"
    head   = "    │ Team             │Pts│  J│  G│  E│  P│ GF│ GC│DIF│"
    sep    = "┌───┼──────────────────┼───┼───┼───┼───┼───┼───┼───┼───┤"
    bottom = "└───┴──────────────────┴───┴───┴───┴───┴───┴───┴───┴───┘"
    in do
      clas <- getClasification
      let criteria = "gc"
       in putStrLn $ unlines $ top:head:sep:(getOrdered criteria clas)++[bottom]