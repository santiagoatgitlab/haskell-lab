lucky :: (Integral a) => a -> String
lucky 7 = "LUCKY NUMBER SEVEN!"
lucky x = "Sorry, you're out of luck, pal"

sayMe :: (Integral a) => a -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe x = "Not between 1 and 5"

factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial x = x * factorial (x-1)

charName 'a' = "Albert"
charName 'b' = "Broseph"
charName 'c' = "Cecil"

length' :: (Num b) => [b] -> b
length' [] = 0
length' (_:xs) = 1 + length' xs

sum' :: (Num b) => [a] -> b
sum' [] = 0
sum' (x:xs) = x + sum' xs
