module Split ( split ) where

split :: String -> Char -> [String]
split [] sep = []
split l sep  = foldl splitInt [""] $ reverse l
    where splitInt [] cur = [[cur]]
          splitInt (last:acc) cur
              | cur == sep = "":last:acc
              | otherwise  = (cur:last):acc
