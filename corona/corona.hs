import Split
import Data.List
import System.Environment

data Country = Country {
    getName      :: String,
    getActive    :: Int,
    getCritical  :: Int,
    get1Mpop     :: Int
    }
    deriving Eq

instance Show Country where
    show (Country n a c m) = let
        pad name    = name ++ (concat $ replicate (32 - length name) " ")
        padNumber n = (concat $ replicate (10 - (length $ show n)) " ") ++ show n 
        in "│" ++ pad n ++ "│" ++ padNumber a ++ "│"
               ++ padNumber c ++ "│" ++ padNumber m ++ "│"

sortByActive :: Country -> Country -> Ordering
sortByActive (Country _ a1 c1 o1) (Country _ a2 c2 o2) = compare (a2,c2,o2) (a1,c1,o1)

sortByCritical :: Country -> Country -> Ordering
sortByCritical (Country _ a1 c1 o1) (Country _ a2 c2 o2) = compare (c2,a2,o2) (c1,a1,o1)

sortBy1MPop :: Country -> Country -> Ordering
sortBy1MPop (Country _ a1 c1 o1) (Country _ a2 c2 o2) = compare (o2,a2,c2) (o1,a1,c1)

sortCountries :: String -> [Country] -> [Country]
sortCountries criteria countries
    | criteria == "critical" = sortBy sortByCritical countries
    | criteria == "1Mpop"    = sortBy sortBy1MPop countries
    | otherwise              = sortBy sortByActive countries

toInt :: String -> Int
toInt "" = -1
toInt s  = read (filter (/=',') s) :: Int

parseLine :: String -> Country
parseLine l = Country name active critical oneMpop
              where name     = sLine !! 0
                    active   = toInt $ sLine !! 1
                    critical = toInt $ sLine !! 2
                    oneMpop  = toInt $ sLine !! 3
                    sLine    = split l ';'

showIndex :: Int -> String
showIndex n = let sn = show n
              in "│" ++ (concat $ replicate (4 - length sn) " ") ++ sn

displayLines :: [Country] -> [String]
displayLines l = map (\(i,c) -> (showIndex i) ++ show c) $ zip [1..224] l

parse :: String -> String -> [String]
parse c sortC = displayLines $ sortCountries sortC $ map parseLine $ fLines
                where fLines = filter (\l -> length l > 5) $ lines c

firstArg :: [String] -> String
firstArg []            = "active"
firstArg (criteria:[]) = if elem criteria ["active","critical","1Mpop"]
                         then criteria
                         else error $ "ordering criteria (" ++ criteria ++ ") not valid"

main = let 
    top         = "     ┌────────────────────────────────┬──────────┬──────────┬──────────┐"
    head        = "     │ Country                        │ Active   │ Critical │ 1Mpop    │"
    sep         = "┌────┼────────────────────────────────┼──────────┼──────────┼──────────┤"
    bottom      = "└────┴────────────────────────────────┴──────────┴──────────┴──────────┘"
    in do
        args     <- getArgs
        contents <- readFile "./finalData"
        let criteria = firstArg args
         in putStrLn $ init $ unlines $ top:head:sep:(parse contents criteria)++[bottom]
