#!/bin/bash

cd ~/lab/haskell-lab/corona
curl https://corona-stats.online > ./data

grep -v "═\|──\|World" ./data |
    sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g" |
    sed "s/\s*//g ;
         s/║//g ;
         s/│/;/g" |
    awk -F';' '{print $2";"$8";"$9";"$10}' > ./finalData

./corona active   > history/active_$(date +"%Y%m%d")
./corona critical > history/critical_$(date +"%Y%m%d")
./corona 1Mpop    > history/1Mpop_$(date +"%Y%m%d")
