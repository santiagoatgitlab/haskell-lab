-- file: ch03/MySecond

mySecond xs = if null (tail xs)
              then error "list too short"
              else head (tail xs)

tidySecond :: [a] -> Maybe a
tidySecond (_:x:_)  = Just 
tidySecond _        = Nothing
