data List a = Cons a (List a)
            | Nil
            deriving(Show)

toList Nil          = []
toList (Cons a b )  = a:(toList b)


data Numero a = Maybe a
data Mtree a = Node a (Maybe (Mtree a)) (Maybe (Mtree a))
