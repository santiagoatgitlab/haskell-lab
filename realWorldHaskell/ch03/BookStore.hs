-- File: BookStore.hs
data BookInfo = Book Int String [String]
                deriving (Show)
myInfo = Book 393939438948 "Algebra of programming" [ "Richard Bird", "Oege De moor" ]
bookID      (Book id title authors) = id
bookTitle   (Book id title authors) = title
bookAuthors (Book id title authors) = authors
