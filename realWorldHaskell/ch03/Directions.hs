data Direction a = LEFT
                 | RIGHT
                 | STRAIGHT
                   deriving (Show)

evaluateBarg a c = if abs (fst c / snd c) > abs (fst a / snd a)
                   then RIGHT
                   else LEFT

evaluateBalf a c = if abs (fst c / snd c) > abs (fst a / snd a)
                   then LEFT
                   else RIGHT

evaluateSimple a c = if (snd a * fst c) > 0 
                     then LEFT
                     else RIGHT

evaluateComplex a c = if (fst a * snd c) <= 0 
                      then evaluateBarg a c
                      else evaluateBalf a c

getRelative x b = (fst x - fst b, snd x - snd b)

evaluateNonStraight rel_a rel_c = if (fst rel_a * snd rel_a * fst rel_c * snd rel_c) < 0
                                  then evaluateSimple rel_a rel_c
                                  else evaluateComplex rel_a rel_c


getDir a b c = let rel_a = getRelative a b
                   rel_c = getRelative c b
               in if (fst a == fst b && fst b == fst c) || (snd a == snd b && snd b == snd c)
                  then STRAIGHT
                  else if (fst rel_c / snd rel_c) == (fst rel_a / snd rel_a)
                       then STRAIGHT
                       else evaluateNonStraight rel_a rel_c

computeDirs (a:b:[])  = []
computeDirs (a:b:c:d) = (getDir a b c) : computeDirs(b:(c:d))
