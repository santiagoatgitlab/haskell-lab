-- Ch03: Final exercises
import Data.List
mean l = let add []       = 0
             add (x:xs)   = x + (add xs)
             count []     = 0
             count (x:xs) = 1 + (count xs)
         in (add l) / fromIntegral (count l)

pali l = l ++ reverse l

isPal l = l == reverse l

ident (x:xs) = "List"
ident b   = "No list"

sortByLength l = let ordFun l1 l2 = if length l1 < length l2
                                    then LT
                                    else GT
                 in sortBy ordFun l

interspace [] _     = ""
interspace (x:xs) delim = let join (x:xs) = [delim] ++ x ++ (join xs)
                              join []     = ""
                          in x ++ (join xs)

data Tree a = Node a (Tree a) (Tree a)
            | Empty
              deriving (Show)

parent (Node p _ _ )       = p
firstChild (Node _ fc _ )  = fc
secondChild (Node _ _ sc ) = sc

countLevels Empty                = 0
countLevels (Node p Empty Empty) = 1
countLevels (Node p n1 n2)       = let oneIfSome a b = if a > 0 || b > 0
                                                       then max a b
                                                       else 0
                                   in 1 + oneIfSome (countLevels n1) (countLevels n2)

ramaE  = Node 3 Empty Empty
ramaA  = Node 2 Empty ramaE
rama1  = Node 4 ramaA ramaE
rama2  = Node 7 Empty ramaE
tronco = Node 5 rama1 rama2

createTree []         = Empty
createTree (x:y:ys)   = Node x (createTree ys) (createTree [y])
createTree (x:xs)     = Node x Empty Empty
-- Fix this function so it creates a proper tree once you know
-- how to cast the result of (length List) to a Fractional

data Direction a = LEFT
                 | RIGHT
                 | STRAIGHT
                   deriving (Show)

getDirs []        = []
getDirs (a:[])    = []
getDirs (a:b:[])  = []
getDirs (a:b:c:d) = [getDirection a b c] : getDirs(b:c:d)
