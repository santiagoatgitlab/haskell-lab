-- lastButOne function that takes an array and returns the element before the last
lastButOne xs = if length xs > 1
		then last (take ((length xs) - 1) xs)
		else last xs 
