-- displays a random integer in a range passed as argument
import System.Random
import System.Environment

strToInt :: String -> Int
strToInt "" = error "not enough argument, expected: min max"
strToInt s  = read s

main = do
    args <- getArgs
    gen  <- getStdGen
    let (min':max':_) = map strToInt args
    putStrLn $ show $ fst $ randomR (min',max') gen
