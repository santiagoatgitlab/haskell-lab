import Data.List

range1 x y = (x < -5) == (y < -5)
range2 x y = (x >= -5 && x <=5) == (y >= -5 && y <= 5) 
range3 x y = (x > 5) == (y > 5)

nuby x y = range1 x y && range2 x y && range3 x y
