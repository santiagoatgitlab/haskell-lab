import qualified Data.Map as Map

findKey :: (Eq k) => k -> [(k,v)] -> v
findKey key xs = snd . head . filter (\(k,v) -> key == k) $ xs

provinces =
    [
        ("RN", "Rio Negro"),
        ("BA", "Buenos Aires"),
        ("N", "Neuquen"),
        ("SC", "Santa Cruz")
    ]

prov  = Map.fromList provinces
prov2 = Map.insert "TDF" "Tierra del fuego" prov
