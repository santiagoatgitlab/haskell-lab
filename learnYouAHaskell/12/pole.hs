type Birds = Int
type Pole  = (Birds,Birds)

landLeft :: Birds -> Pole -> Maybe Pole
landLeft n (left, right)
    | abs (newLeft - right) <= 3 = Just (newLeft,right)
    | otherwise                  = Nothing
    where newLeft = left + n

landRight :: Birds -> Pole -> Maybe Pole
landRight n (left, right)
    | abs (newRight - left) <= 3 = Just (newRight,left)
    | otherwise                  = Nothing
    where newRight = right + n

routine :: Maybe Pole
routine = do
    start <- Just (0,0)
    first <- landLeft 1 start
    second <- landRight 1 first
    landLeft 2 second
    
