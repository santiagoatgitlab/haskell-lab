-- A knights's quest

type KnightPos = (Int,Int)

allMoves = [ (1,2), (2,1), (-1,2), (-2,1), (-1,-2), (-2,-1), (1,-2), (2,-1) ]

getMove :: KnightPos -> KnightPos -> KnightPos
getMove (x1,y1) (x2,y2) = (x1+x2,y1+y2)

moveKnight :: [KnightPos] -> [KnightPos]
moveKnight kp = getMove <$> allMoves <*> kp
