largestMultiple :: (Integral n) => n -> n -> n
largestMultiple n ceil
    | ceil `mod` n == 0 = ceil
    | otherwise         = largestMultiple n (ceil-1)

