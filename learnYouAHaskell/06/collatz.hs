collatz :: (Integral a) => a -> [a]
collatz n
   | n == 1 = [1]
   | even n = n : collatz (n `div` 2)
   | odd n  = n : collatz (n*3 + 1)
howCollatz n = length (filter (>15) (map length (map collatz [1..n] )))
