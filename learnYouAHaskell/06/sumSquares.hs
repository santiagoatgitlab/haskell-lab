sumSquaresSmallerThan :: (Integral n) => n -> n
sumSquaresSmallerThan n = sum (map (^2) (filter odd [0..n]))
