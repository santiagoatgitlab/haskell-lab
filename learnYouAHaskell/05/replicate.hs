replicate' :: Int -> a -> [a]
replicate' n e 
    | n == 0    = []
    | otherwise = e:(replicate' (n-1) e)

