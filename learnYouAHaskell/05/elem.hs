elem' n [] = False
elem' n (x:xs)
    | n == x    = True
    | otherwise = elem' n xs
