maximum' list = case list of []     -> error "The list is empty"
                             [a]    -> a
                             (x:xs) -> max x (maximum' xs)
