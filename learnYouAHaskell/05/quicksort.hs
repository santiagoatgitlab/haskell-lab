quicksort :: (Ord a) => [a] -> [a]
quicksort []     = []
quicksort (x:xs) = let filterBigger  = quicksort (filter (>x) xs)
                       filterSmaller = quicksort (filter (<=x) xs)
                   in filterSmaller ++ [x] ++ quicksort filterBigger
