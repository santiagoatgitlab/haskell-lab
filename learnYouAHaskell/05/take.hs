-- take'

take' a _
    | a <= 0   = []
take' _ []     = [] 
take' a (x:xs) = x : take' (a-1) xs
