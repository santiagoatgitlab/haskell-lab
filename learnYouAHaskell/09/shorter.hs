main = do
    l <- getContents
    putStrLn $ unlines $ filter (\line -> length line < 10) $ lines l

