respondPalindrom = unlines . map (\xs -> if isPalindrome xs
                                  then "palindrome"
                                  else "not a palindrome") . lines
                   where isPalindrome word = reverse word == word

main = interact respondPalindrom
