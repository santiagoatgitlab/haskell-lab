import Data.Monoid

instance Monoid Int where
    mempty = 1
    mappend a b = (*)
