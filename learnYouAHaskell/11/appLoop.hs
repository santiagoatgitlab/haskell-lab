loop :: Int -> [Int] -> [Int]
loop n l
    | n == 0 = l
    | otherwise = loop (n-1) (a <*> l)
    where a = [(+1),(+4)]
