loop :: Int -> Maybe Int -> Maybe Int
loop n l
    | n == 0 = l
    | otherwise = loop (n-1) (a <*> l)
    where a = Just (+3)
