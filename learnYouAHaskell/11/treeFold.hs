import qualified Data.Foldable as F

data Tree a = Empty | Node a (Tree a) (Tree a) deriving Show

instance F.Foldable Tree where
    foldMap f Empty = mempty
    foldMap f (Node x l r) = F.foldMap f l `mappend`
                             f x           `mappend`
                             F.foldMap f r

s  = Node "s" Empty Empty
n  = Node "n" Empty Empty
i  = Node "i" Empty Empty
g  = Node "g" Empty Empty
a3 = Node "a" Empty Empty
e  = Node "e" Empty Empty

a  = Node "a" s n
a2 = Node "a" i g
o  = Node "o" g n
l  = Node "l" a3 e

t = Node "t" a a2
z = Node "z" o l

o2 = Node "o" t z
