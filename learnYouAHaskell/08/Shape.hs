data Point = Point Float Float
           deriving (Show)
data Shape = Circle Point Float 
           | Rect Point Point
           deriving (Show)

surface :: Shape -> Float
surface (Circle _ r) = 2 * 3.14 * r
surface (Rect (Point aX aY) (Point bX bY)) = abs (aX - bX) * abs (aY - bY)
