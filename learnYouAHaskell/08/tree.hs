data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)

getNodeValue (Node nv _ _ ) = nv
getLeftNode (Node _ ln _)   = ln
getRightNode (Node _ _ rn)  = rn

treeInsert EmptyTree nv = Node nv EmptyTree EmptyTree
treeInsert (Node v l r) nv 
    | nv == v = Node v l r 
    | nv > v  = Node v l (treeInsert r nv)
    | nv < v  = Node v (treeInsert l nv) r

isInTree EmptyTree e = False
isInTree (Node v l r) e
    | e == v = True
    | e > v  = isInTree r e
    | e < v  = isInTree l e

instance Functor Tree where
    fmap f EmptyTree    = EmptyTree
    fmap f (Node v l r) = (Node (f v) (fmap f l) (fmap f r))

getLeftExtreme :: (Tree a) -> a
getLeftExtreme (Node v EmptyTree _) = v
getLeftExtreme (Node v l _)         = getLeftExtreme l

getRightExtreme :: (Tree a) -> a
getRightExtreme (Node v _ EmptyTree) = v
getRightExtreme (Node v _ r)         = getLeftExtreme r

a = Node 137 EmptyTree EmptyTree
b = treeInsert a 12
c = treeInsert b 63
d = treeInsert c 89
e = treeInsert d 500
f = treeInsert e 201
