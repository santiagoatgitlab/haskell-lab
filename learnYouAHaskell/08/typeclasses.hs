data WeekDay = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday 
    deriving (Eq, Ord, Enum)

instance Show WeekDay where
    show Monday    = "Lunes"
    show Tuesday   = "Martes"
    show Wednesday = "Miércoles"
    show Thursday  = "Jueves"
    show Friday    = "Viernes"
    show Saturday  = "Sabado"
    show Sunday    = "Domingo"
