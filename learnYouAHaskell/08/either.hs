data Pet = Cat | Dog deriving (Show, Eq)

myPets = [ Left Cat, Left Dog, Right "bird", Right "turtle" ]

getPet (Left a)  = show a
getPet (Right b) = b
