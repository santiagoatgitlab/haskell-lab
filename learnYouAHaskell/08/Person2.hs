data Person2 = Person2 { 
                           firstName   :: String,
                           lastName    :: String,
                           age         :: Int,
                           height      :: Float,
                           phoneNumber :: String,
                           iceCream    :: String
                       } deriving (Show)
