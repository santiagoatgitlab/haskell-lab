import qualified Data.Map as Map

instance Functor (Map.Map k) where
    fmap f (Map.fromList [])     = Map.fromList []
    fmap f (Map.fromList [(k,v)] = Map.fromList [(k,f v)]
