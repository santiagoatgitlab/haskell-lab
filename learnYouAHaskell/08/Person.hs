type FirstName   = String
type LastName    = String
type Age         = Int
type Height      = Float
type PhoneNumber = String
type IceCream    = String

data Name   = Name FirstName LastName deriving (Show)
data Person = Person Name Age Height PhoneNumber IceCream deriving (Show)

getFirstName (Name f _) = f
getLastName (Name _ l)  = l

getName (Person n _ _ _ _)     = n
getAge (Person _ a _ _ _)      = a
getHeight (Person _ _ h _ _)   = h
getPhone (Person _ _ _ p _)    = p
getIceCream (Person _ _ _ _ i) = i
