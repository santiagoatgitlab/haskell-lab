toFunction a = case a of
                   "+"   -> (+)
                   "*"   -> (*)
                   "-"   -> (-)
                   "div" -> div

processNextItem stack newItem
    | elem newItem operators = (take (len-2) stack) ++ [ show newResult ]
    | otherwise              = stack ++ [newItem]
    where newResult = (toFunction newItem) (fst lastTwo) (snd lastTwo)
          lastTwo   = (read (stack !! (len-2)) :: Int, read (stack !! (len-1)) :: Int)
          operators = ["+","-","*","div","negate"]
          len       = length stack

polishSolve :: String -> Either Int String
polishSolve exp = let result = foldl processNextItem [] $ words exp
                  in if ((length result) == 1)
                     then Left (read (head result) :: Int)
                     else Right "Badly formed expression"

polishToString exp = let parseEither (Left a)  = show a
                         parseEither (Right a) = a
                     in parseEither (polishSolve exp)

polishToInteger exp = let parseEither (Left a)  = a
                          parseEither (Right a) = 0
                      in parseEither (polishSolve exp)
