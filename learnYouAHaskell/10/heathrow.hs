data Node = A Int | B Int deriving (Show)

nextNode (a1,b1,c1) (prevNode:_) = (valForA a1 c1 prevNode,valForB b1 c1 prevNode)
    where getA (a, _)   = getVal a
          getB (_, b)   = getVal b
          getVal (A v)  = v
          getVal (B v)  = v
          valForA a c n = getNode (a + getA n) (a + c + getB n)
          valForB b c n = getNode (b + c + getA n) (b + getB n) 
          getNode toA toB
              | toA < toB = A toA
              | otherwise = B toB

getMinor (a,b)
    | aIsMinor  = 'a'
    | otherwise = 'b'
    where aIsMinor = unpackNode a < unpackNode b
          unpackNode (A x) = x
          unpackNode (B x) = x

toGetter side
    | side == 'a' = fst
    | side == 'b' = snd

getHeadA (a,_,_) = a
getHeadB (_,b,_) = b
toLetter (A _)   = 'a'
toLetter (B _)   = 'b'

aPoints = [12,27,10,19,33,41,38,46,41]
bPoints = [24,31,37,33,55,59,58,50,14]
cross   = [55,49,47,42,2,53,5,16,0]

list :: [(Int,Int,Int)]
list = reverse $ zip3 aPoints bPoints cross
orig = [(A (getHeadA $ head list), B (getHeadB $ head list))]
res  = foldl (\acc t -> (nextNode t acc):acc) orig (tail list)
path = reverse $ foldl (\(h:t) c -> (toLetter $ (toGetter h) c):h:t) [getMinor $ head res] res
