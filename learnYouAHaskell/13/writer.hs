join :: [String] -> String
join ("":xs) = join xs
join (x:[]) = x
join (x:xs) = x ++ ", " ++ (join xs)

plus :: Int -> (Int, String)
plus n = (n + 9, "adding 9 to " ++ (show n))

subs :: Int -> (Int, String)
subs n = (n - 9, "substracting 9 from " ++ (show n))

lift :: a -> (a, String)
lift n = (n,"")

(+++) :: (a, String) -> (a -> (a, String)) -> (a, String)
(v,l) +++ f = (newValue, join [l, newLog])
    where (newValue, newLog) = f v
