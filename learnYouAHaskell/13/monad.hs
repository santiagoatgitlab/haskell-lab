import Control.Monad.Writer

plus :: Int -> Writer String Int
plus n = Writer (n + 9, "adding 9 to " ++ (show n))

subs :: Int -> Writer String Int
subs n = Writer (n - 9, "substracting 9 from " ++ (show n))

a = return 7 :: Writer String Int
