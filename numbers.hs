data Number = One | Two | Three | Four | Five | Six | Seven
              deriving (Eq, Show, Ord)

instance Num Number where
    (+) One Two = Seven
